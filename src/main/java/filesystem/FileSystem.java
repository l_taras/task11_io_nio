package filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileSystem {

    private File file = new File("src");

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public static void main(String[] args) {
        FileSystem manager = new FileSystem();

        manager.dir();
        manager.cd("main");
        manager.dir();
        manager.cd("java");
        manager.dir();
        manager.cd("ship");
        manager.dir();
    }

    private void dir() {
        File[] arr = file.listFiles();
        if (file.getParent() != null) {
            System.out.println(file.getParent() + "...");
        }
        List<File> files = new ArrayList<>(Arrays.asList(arr));
        for (File file1 : files) {
            System.out.print(file1.getName() + " canRead= " + file1.canRead()
                    + " canWrite= " + file1.canWrite()
                    + "is Directory " + file1.isDirectory());
            System.out.println();
        }
        System.out.println();
    }

    private void cd(String name) {

        file = new File(file.getAbsolutePath() + "\\" + name);
    }

}

