package ship;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Manager {
    private static File file = new File("src\\main\\resources\\ship\\file.ser");


    public static void main(String[] args) {
        Droid d1 = new Droid("robo1", "a");
        Droid d2 = new Droid("robo2", "b");
        Droid d3 = new Droid("robo3", "c");
        Ship s1 = new Ship(d1, "m1", 50);
        Ship s2 = new Ship(d2, "m2", 100);
        List<Ship> ships = new ArrayList<>();
        ships.add(s1);
        ships.add(s2);
        writeInto(ships);

        List<Ship> readShips = new ArrayList<>();
        readShips = readFrom();
        for (Ship readShip : readShips) {
            System.out.println(readShip);

        }
    }

    public static List<Ship> readFrom() {
        ObjectInputStream istream = null;
        List<Ship> arr = new ArrayList<>();
        try {
            istream = new ObjectInputStream(new FileInputStream(file));
            arr = (List<Ship>) istream.readObject();
            istream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arr;
    }

    public static void writeInto(List<Ship> arr) {
        try {
            ObjectOutputStream ostream = null;
            ostream = new ObjectOutputStream(new FileOutputStream(file));
            ostream.writeObject(arr);
            ostream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
