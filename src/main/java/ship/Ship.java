package ship;

import java.io.Serializable;
import java.util.StringJoiner;

public class Ship implements Serializable {
    Droid droid;
    String model;
    transient Integer number;

    public Ship(Droid droid, String model, Integer number) {
        this.droid = droid;
        this.model = model;
        this.number = number;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ship.class.getSimpleName() + "[", "]")
                .add("droid=" + droid)
                .add("model='" + model + "'")
                .add("number=" + number)
                .toString();
    }
}
