package ship;

import java.io.Serializable;
import java.util.StringJoiner;

public class Droid implements Serializable {
    private String name;
    private transient String password;

    public Droid(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Droid.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("password='" + password + "'")
                .toString();
    }
}

