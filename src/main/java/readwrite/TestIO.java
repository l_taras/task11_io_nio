package readwrite;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;

public class TestIO {
    public static void main(String[] args) throws Exception {
        System.out.println("start copy with no buffer");
        copyFileNoBuffer();
        System.out.println("finished");

        System.out.println("start copy with buffer 16");
        copyFileWithBuffer(16);
        System.out.println("finished");

        System.out.println("start copy with buffer 4096");
        copyFileWithBuffer(4096);
        System.out.println("finished");

        System.out.println("start copy with buffer 65536");
        copyFileWithBuffer(65536);
        System.out.println("finished");

    }

    private static void copyFileNoBuffer() {
        try (Reader is = new FileReader("src\\main\\resources\\readwrite\\text.pdf");
             Writer os = new FileWriter("src\\main\\resources\\readwrite\\textcopy.pdf")) {
            int r = is.read();
            while (r != -1) {
                r = is.read();
                os.write(r);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void copyFileWithBuffer(int bufferSize) {
        try (Reader r = new FileReader("src\\main\\resources\\readwrite\\text.pdf");
             Writer w = new FileWriter("src\\main\\resources\\readwrite\\textcopy.pdf")) {
            char[] buffer = new char[bufferSize];
            int c = r.read(buffer);
            while (c != -1) {
                w.write(buffer, 0, c);
                c = r.read(buffer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

